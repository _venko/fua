require("stack")
require("interpreter")

P00 = "1 2 3 4 .s"
P01 = "1 2 + 3 4 + + 2 / 1000 * 3 % .s"
P02 = "1 dup 2 swap .s"
P03 = "1 2 over .s"
P04 = "true true and false and true or .s"
P05 = "4 5 + 5 4 + = .s"
P06 = "5 . .s"
P07 = "[ foo bar ]  [ bink bonk ] .s swap .s"
P08 = "[ 1 2 + ] dup eval swap eval + ."
P09 = "false          \
            if [ 1 2 + ]   \
            else [ 4 5 + ] \
            then .s eval .s"
P10 = "0 10 1 do drop 1 + loop .s"
P11 = "0 10 1 do + loop .s"
P12 = "0 10 1 do dup 2 % 0 = if + else drop then loop .s"
P13 = ": nums 1 2 3 ; nums .s"
P14 = [[

: -rot rot rot ;
: nums 1 do loop ;
: sum 1 0 -rot do drop + loop ;
100 nums dup sum .]]

PS = {P00, P01, P02, P03, P04, P05,
      P06, P07, P08, P09, P10, P11,
      P12, P13, P14}

function main()
  for _,program in pairs(PS) do
    local interp = Interpreter:Create()
    interp.tracing = false
    io.write("PROGRAM: ", program, " ")
    if interp.tracing then io.write("\nRunning with tracing on...\n") end
    interp:run(program)
    io.write("ok\n\n")
  end
end

main()
