# Fua: A Simple Forth dialect written in Lua
I wanted to see just how cheap it is to get a working Forth up and running.
For this I decided to use Lua, since it's a language I've been meaning to learn
lately.

# TODO
* [ ] Clean up the entire codebase lol

## Demo
The project currently supports a tiny handful of words.
Here are some example executions:
```
PROGRAM: 1 2 + 3 4 + + 2 / 1000 * 3 % .s 
Running with tracing on...
1 
1 2 
1 2 + 
3 
3 3 
3 3 4 
3 3 4 + 
3 7 
3 7 + 
10 
10 2 
10 2 / 
5 
5 1000 
5 1000 * 
5000 
5000 3 
5000 3 % 
2 
2 .s 
<1> 2 ok

PROGRAM: 1 dup 2 swap .s 
Running with tracing on...
1 
1 dup 
1 1 
1 1 2 
1 1 2 swap 
1 2 1 
1 2 1 .s 
<3> 1 2 1 ok

PROGRAM: 1 2 over .s 
Running with tracing on...
1 
1 2 
1 2 over 
1 2 1 
1 2 1 .s 
<3> 1 2 1 ok

PROGRAM: true true and false and true or .s 
Running with tracing on...
true 
true true 
true true and 
true 
true false 
true false and 
false 
false true 
false true or 
true 
true .s 
<1> true ok

PROGRAM: 4 5 + 5 4 + = .s 
Running with tracing on...
4 
4 5 
4 5 + 
9 
9 5 
9 5 4 
9 5 4 + 
9 9 
9 9 = 
true 
true .s 
<1> true ok

PROGRAM: 5 . .s 
Running with tracing on...
5 
5 . 
5 
.s 
<0> ok
```

