-- Debugging
local function show_drop(machine)
  local a = machine:pop()
  if type(a) == "table" then
    io.write("[ ")
    for _, elem in ipairs(a) do io.write(elem, " ") end
    io.write("] ")
  else io.write(a, " ") end
end

local function show_stack(machine)
  io.write("<", machine._stack:getn(), "> ")
  machine:disp()
end
-- End debugging

-- Stack manipulation
local function drop(machine)
  machine:pop()
end

local function dup(machine)
  local a = machine:pop()
  machine:push(a, a)
end

local function swap(machine)
  local b, a = machine:pop(2)
  machine:push(b, a)
end

local function over(machine)
  b, a = machine:pop(2)
  machine:push(a, b, a)
end

local function rot(machine)
  local c, b, a = machine:pop(3)
  machine:push(b, c, a)
end
-- End stack manipulation

-- Boolean
local function bool_not(machine)
  local a = machine.pop()
  machine:push(not a)
end

local function bool_and(machine)
  local b, a = machine:pop(2)
  machine:push(a and b)
end

local function bool_or(machine)
  local b, a = machine:pop(2)
  machine:push(a or b)
end
-- End boolean

-- Comparison
local function eq(machine)
  local b, a = machine:pop(2)
  machine:push(a == b)
end

local function ne(machine)
  local b, a = machine:pop(2)
  machine:push(a ~= b)
end

local function lt(machine)
  local b, a = machine:pop(2)
  machine:push(a < b)
end

local function le(machine)
  local b, a = machine:pop(2)
  machine:push(a <= b)
end

local function gt(machine)
  local b, a = machine:pop(2)
  machine:push(a > b)
end

local function ge(machine)
  local b, a = machine:pop(2)
  machine:push(a >= b)
end
-- End comparison

-- Arithmetic
local function add(machine)
  local b, a = machine:pop(2)
  machine:push(a + b)
end

local function sub(machine)
  local b, a = machine:pop(2)
  machine:push(a - b)
end

local function mul(machine)
  local b, a = machine:pop(2)
  machine:push(a * b)
end

local function div(machine)
  local b, a = machine:pop(2)
  machine:push(a // b)
end

local function mod(machine)
  local b, a = machine:pop(2)
  machine:push(a % b)
end
-- End arithmetic

-- Quotation
local function evaluate(machine)
  local quote = machine:pop()
  local interpreter = Interpreter:Create(machine)
  interpreter:run(table.concat(quote, " "))
end

local function traced_eval(machine)
  local quote = machine:pop()
  local interpreter = Interpreter:Create(machine)
  interpreter.tracing = true
  interpreter:run(table.concat(quote, " "))
end
-- End quotation

return {
  [".s"] = show_stack,
  ["."] = show_drop,

  ["drop"] = drop,
  ["dup"] = dup,
  ["swap"] = swap,
  ["over"] = over,
  ["rot"] = rot,

  ["not"] = bool_not,
  ["and"] = bool_and,
  ["or"] = bool_or,

  ["="] = eq,
  ["<>"] = ne,
  ["<"] = lt,
  [">"] = gt,
  ["<="] = le,
  [">="] = gt,

  ["+"] = add,
  ["-"] = sub,
  ["*"] = mul,
  ["/"] = div,
  ["%"] = mod,

  ["eval"] = evaluate,
  ["eval_trace"] = traced_eval,
}
