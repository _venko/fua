Machine = {}

function Machine:Create()
  local t = {}
  t._stack = Stack:Create()
  t._dict = {}

  function t:push(...)
    t._stack:push(...)
  end

  function t:pop(n)
    return t._stack:pop(n)
  end

  function t:list()
    io.write("WARN: Machine:list is deprecated. Use Machine:disp instead\n")
    t._stack:list()
  end

  function t:disp()
    function print_table(tbl)
      for _, v in pairs(tbl) do
        if type(v) == "table" then
          io.write("[ "); print_table(v); io.write("]")
        else
          io.write(tostring(v))
        end
        io.write(" ")
      end
    end
    print_table(self._stack._et)
  end

  -- Loads a table of word->function pairs into this interpreter's dictionary.
  -- Colliding words will be overwritten with the new definition.
  function t:load(defns)
    for k,v in pairs(defns) do self._dict[k] = v end
  end

  return t
end
