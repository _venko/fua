Stack = {}

function Stack:Create()
  local t = {}
  t._et = {}

  function t:push(...)
    if ... ~= nil then
      local targs = {...}
      for _,v in ipairs(targs) do
        table.insert(self._et, v)
      end
    end
  end

  function t:pop(n)
    local n = n or 1
    local entries = {}
    for i = 1, n do
      if #self._et ~= 0 then
        table.insert(entries, self._et[#self._et])
        table.remove(self._et)
      else
        break
      end
    end
    return table.unpack(entries)
  end

  function t:getn()
    return #self._et
  end

  function t:list()
    if #self._et ~= 0 then
      for _,v in ipairs(self._et) do
        io.write(tostring(v), " ")
      end
    else
    end
  end

  return t
end
