require("stack")
require("machine")

Interpreter = {}

function Interpreter:Create(machine)
  local t = {}
  t.tracing = false

  if not machine then
    local core = require("builtins-core")
    t.machine = Machine:Create()
    t.machine:load(core)
  else t.machine = machine end

  local function try_push_bool(token)
    local toboolean = {
      ["true"] = true,
      ["false"] = false,
    }
    if toboolean[token] ~= nil then
      t.machine:push(toboolean[token])
      return true
    end
    return false
  end

  local function try_push_int(token)
    local n = tonumber(token, 10)
    if not n then return false end
    t.machine:push(n)
    return true
  end

  local function try_push_word(token)
    if not t.machine._dict[token] then
      io.write("Unrecognized token: ", token, "\n")
      return false
    end
    t.machine:push(token)
    return true
  end

  local function parse_token(token)
    if try_push_int(token) then return "literal" end
    if try_push_bool(token) then return "literal" end
    if try_push_word(token) then return "word" end
    return nil
  end

  local function call_word()
    local word = t.machine:pop()
    local func = t.machine._dict[word]
    if not func then
      io.write("Failed to invoke `", word, "`: ", err, "\n")
      return
    end
    func(t.machine)
  end

  local function show_trace()
    io.write("<", #t.machine._stack._et, "> ")
    t.machine:disp()
    io.write("\n")
  end

  local function consume_until(stream, marker)
    local tbl = {}
    local token = stream()
    while token and token ~= marker do
      tbl[#tbl+1] = token
      token = stream()
    end
    return tbl
  end

  local function get_if_branch(condition, stream)
    local capture = consume_until(stream, "then")
    local i = 1

    local true_branch = {}
    while i < #capture do
      local t = capture[i]
      if t == "else" then break end
      true_branch[#true_branch+1] = t
      i = i + 1
    end

    if condition == true then return true_branch end

    local false_branch = {}
    while i < #capture do
      i = i + 1
      false_branch[#false_branch+1] = capture[i]
    end
    return false_branch
  end

  function t:make_word(name, quote)
    return {
      [name] = function ()
        self.machine:push(quote)
        if self.tracing then
          self.machine:push("eval_trace")
        else
          self.machine:push("eval")
        end
        call_word()
      end
    }
  end

  function t:run(program)
    local started = false
    local stream = program:gmatch("%S+")
    local token = stream()

    while token do
      if started and self.tracing then show_trace() end

      -- Handle quotations
      if token == "[" then
        local quote = consume_until(stream, "]")
        self.machine:push(quote)
      elseif token == "if" then
        -- Handle IFs by determining which branch the program should take,
        -- pushing the entire branch as if it's a quotation, and then
        -- immediately evaluating the quotation.
        -- This is a bit hacky but it saves us the trouble of pretending to
        -- backtrack the lex cursor.
        local condition = t.machine:pop()
        local branch = get_if_branch(condition, stream)
        t.machine:push(branch)
        if self.tracing then
          self.machine:push("eval_trace")
        else
          self.machine:push("eval")
        end
        call_word()
      elseif token == "do" then
        local quote = consume_until(stream, "loop")
        local lo, hi = self.machine:pop(2)
        local unrolled = {}
        for i=lo, hi do
          unrolled[#unrolled+1] = i
          for _, elem in ipairs(quote) do
            unrolled[#unrolled+1] = elem
          end
        end
        self.machine:push(unrolled)
        if self.tracing then
          self.machine:push("eval_trace")
        else
          self.machine:push("eval")
        end
        call_word()
      elseif token == ":" then
        token = stream()
        local name = token
        local quote = consume_until(stream, ";")
        local word = self:make_word(name, quote)
        self.machine:load(word)
      else
        local parse_result = parse_token(token)
        if not parse_result then
          io.write("Error: failed to parse token ", token, "\n")
          return
        end

        -- All words are immediate right now.
        if parse_result == "word" then
          if started and self.tracing then show_trace() end
          local word = self.machine:pop()
          local needs_newline = false
          if self.tracing then
            if word == "eval" then
              word = "eval_trace"
            elseif word == ".s" then
              needs_newline = true
            end
          end
          self.machine:push(word)
          call_word()
          if self.tracing and needs_newline then io.write("\n") end
        end
      end
      started = true
      token = stream()
    end
  end

  return t
end
